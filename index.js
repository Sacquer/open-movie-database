const axios = require('axios')

const GET = 'GET'
const SERIES = 'series'
const MOVIE = 'movie'
const URL = 'http://www.omdbapi.com'

class OpenMovieDatabase {
  constructor(apiKey) {
    this.apiKey = apiKey
  }

  async search(parameters) {
    try {
      const { data } = await axios({
        method: GET,
        url: URL,
        params: { apikey: this.apiKey, ...parameters },
      })
      return data
    } catch (error) {
      console.log(error.response)
      throw error.response.data
    }
  }

  async get(parameters) {
    try {
      const { data } = await axios({
        method: GET,
        url: URL,
        params: { apikey: this.apiKey, ...parameters },
      })
      return data
    } catch (error) {
      console.log(error.response)
      throw error.response.data
    }
  }

  async getSeriesByTitle(title) {
    try {
      const { data } = await axios({
        method: GET,
        url: URL,
        params: { apikey: this.apiKey, t: title, type: SERIES },
      })
      return data
    } catch (error) {
      console.log(error.response)
      throw error.response.data
    }
  }

  async getMovieByTitle(title) {
    try {
      const { data } = await axios({
        method: GET,
        url: URL,
        params: { apikey: this.apiKey, t: title, type: MOVIE },
      })
      return data
    } catch (error) {
      console.log(error.response)
      throw error.response.data
    }
  }
}

module.exports = OpenMovieDatabase
