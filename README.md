### API for Open Movie Database

Use like so:

```js
const OpenMovieDatabase = require('@sacquer/omdb-api)
const omdb = new OpenMovieDatabase('your-api-key')

omdb.search({ s: "Succession", type: "series", y: "2018" })
  .then(result => console.log(result))
  .catch(error => console.log(error)

omdb.get({ t: 'Succession', type: "series", y: "2018", plot: "full" })
  .then((result) => console.log(result) )
  .catch((error) => console.log({ error }) )

omdb.getSeriesByTitle('Title of series')
  .then((result) => {
    console.log(result)
  })
  .catch((error) => {
    console.log(error)
  })

omdb.getMovieByTitle('Title of movie')
  .then((result) => {
    console.log(result)
  })
  .catch((error) => {
    console.log(error)
  })
```

